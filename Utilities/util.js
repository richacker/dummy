let config = require("./config").config,
    mustache = require('mustache'),
    bodyParser = require('body-parser'),
    MD5 = require("md5");
var querystring = require('querystring');


let encryptData = (stringToCrypt) => {
    return MD5(MD5(stringToCrypt));
};

// Define Error Codes
let statusCode = {
    ZERO: 0,
    ONE: 1,
    TWO: 2,
    THREE: 3,
    FOUR: 4,
    FIVE: 5,
    SIX: 6,
    SEVEEN: 7,
    EIGHT: 8,
    NINE: 9,
    TEN: 10,
    OK: 200,
    FOUR_ZERO_FOUR: 404,
    BAD_REQUEST: 404,
    FIVE_ZERO_ZERO: 500
};

// Define Error Messages
let statusMessage = {
    PARAMS_MISSING: 'Mandatory Fields Missing',
    SERVER_BUSY: 'Our Servers are busy. Please try again later.',
    PAGE_NOT_FOUND: 'Page not found', //404
    INTERNAL_SERVER_ERROR: 'Internal server error.', //500
    SOMETHING_WENT_WRONG: 'Something went wrong.',
    FETCHED_SUCCESSFULLY: 'Fetched Data Successfully.',
    UPLOAD_SUCCESSFUL: 'Uploaded Image Successfully.',
    EXPENSE_ADDED: "Added Expenses Successfully",
    USER_ADDED: "User created successfully.",
    USER_EXISTS: "User already exists for provided username."
};

let getMysqlDate = (rawDate) => {
    let date = new Date(rawDate);
    return date.getUTCFullYear() + '-' +
        ('00' + (date.getUTCMonth() + 1)).slice(-2) + '-' +
        ('00' + date.getUTCDate()).slice(-2);
}


module.exports = {
    statusCode: statusCode,
    statusMessage: statusMessage,
    getMysqlDate: getMysqlDate,
    encryptData: encryptData
}
