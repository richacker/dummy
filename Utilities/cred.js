let serverURLs = {
    "dev": {
        "NODE_SERVER": "https://localhost",
        "NODE_SERVER_PORT": "3001",
        "MYSQL_HOST": 'localhost',
        "MYSQL_USER": 'root',
        "MYSQL_PASSWORD": 'appstudioz',
        'MYSQL_DATABASE': 'etravel'
    },
    "production": {
        "NODE_SERVER": "https://localhost",
        "NODE_SERVER_PORT": "3001",
        "MYSQL_HOST": 'w3studiozlive.ctg1crr0d6w2.us-east-1.rds.amazonaws.com',
        "MYSQL_USER": 'w3studioz',
        "MYSQL_PASSWORD": 'S!thu89W',
        'MYSQL_DATABASE': 'etravel'
    }
}

module.exports = {
    serverURLs: serverURLs
}