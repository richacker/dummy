'use strict';

let dbConfig = require("../Utilities/dbConfig");

let createUser = (dataToSet, callback) => {
    dbConfig.getDB().query("insert into Users set ? ", dataToSet, callback);
}

let getUsers = (criteria, callback) => {
    let conditions = "";
    criteria.email ? conditions += ` and email = '${criteria.email}'` : true;
    criteria.username ? conditions += ` and username = '${criteria.username}'` : true;
    criteria.password ? conditions += ` and password = '${criteria.password}'` : true;
    dbConfig.getDB().query(`select * from Users where 1 ${conditions}`, callback);
}

module.exports = {
    createUser: createUser,
    getUsers: getUsers
}
