let dbConfig = require("../Utilities/dbConfig");

let initialize = () => {
    dbConfig.getDB().query("create table IF NOT EXISTS Users (User_id INT auto_increment primary key, Username VARCHAR(50), Email VARCHAR(50) UNIQUE, Password VARCHAR(50))");

    //dbConfig.getDB().query("create table IF NOT EXISTS Uploads (User_id INT auto_increment primary key, Username VARCHAR(50), Email VARCHAR(50), filename TEXT)");

  }

module.exports = {
    initialize: initialize
}
