let app = require('express')(),
    bodyParser = require('body-parser');

require('./Utilities/dbConfig');

let userRoute = require('./Routes/user'),
    util = require('./Utilities/util'),
    config = require("./Utilities/config").config;

app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(bodyParser.json());

app.use(function(err, req, res, next) {
    return res.send({ "errorCode": util.statusCode.FOUR_ZERO_ZERO, "errorMessage": util.statusMessage.SOMETHING_WENT_WRONG });
});

app.use('/user', userRoute);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    res.send({ "errorCode": util.statusCode.FOUR_ZERO_FOUR, "errorMessage": util.statusMessage.PAGE_NOT_FOUND })
});


/*first API to check if server is running*/
app.get('/', function(req, res) {
    res.send('hello, world!');
});

app.listen(config.NODE_SERVER_PORT.port);
