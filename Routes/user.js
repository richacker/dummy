let express = require('express'),
    router = express.Router(),
    util = require('../Utilities/util'),
    userService = require('../Services/user');


let multer = require('multer'),
    path = require('path');

let storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, path.resolve("uploads"))
    },
    filename: function(req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
})

/* User Login. */
router.post('/login', (req, res) => {
    userService.signup(req.body, (data) => {
        res.send(data);
    });
});

var upload = multer({ storage: storage });
/* User Login. */
router.post('/upload', upload.single('video'), (req, res) => {
    res.send({ "statusCode": util.statusCode.OK, "statusMessage": "Uploaded Successfully" })
});

module.exports = router;

//2017-05-10T17:12:37.636930Z 1 [Note] A temporary password is generated for root@localhost: X_ZL*?hKh4c#