let async = require('async'),
    queryString = require('querystring');

let util = require('../Utilities/util'),
    AWSUtil = require('../Utilities/AWSUtil'),
    userDAO = require('../DAO/userDAO');

let signup = (data, callback) => {
    async.auto({
        checkUserExistsinDB: (cb) => {
            console.log(data);
            if (!data.username) {
                cb(null, { "statusCode": util.statusCode.BAD_REQUEST, "statusMessage": util.statusMessage.PARAMS_MISSING })
                return;
            }

            if (!data.email) {
                cb(null, { "statusCode": util.statusCode.BAD_REQUEST, "statusMessage": util.statusMessage.PARAMS_MISSING })
                return;
            }
            var criteria = {
                email: data.email
            }
            userDAO.getUsers(criteria, (err, dbData) => {
                console.log(dbData);
                if (err) {
                    cb(null, { "statusCode": util.statusCode.INTERNAL_SERVER_ERROR, "statusMessage": util.statusMessage.FAILED })
                    return;
                }

                if (dbData && dbData.length) {
                    cb(null, { "statusCode": util.statusCode.BAD_REQUEST, "statusMessage": util.statusMessage.USER_EXISTS, "data": dbData[0] });
                } else {
                    cb(null);
                }
            });
        },
        createUserinDB: ['checkUserExistsinDB', (cb, functionData) => {
            if (functionData && functionData.checkUserExistsinDB && functionData.checkUserExistsinDB.data) {
                if(functionData.checkUserExistsinDB.data.Username == data.username && functionData.checkUserExistsinDB.data.Password == util.encryptData(data.password)){
                    cb(null, { "statusCode": util.statusCode.OK, "statusMessage": "EXISTS", data: {"email": functionData.checkUserExistsinDB.data.Email, "username": functionData.checkUserExistsinDB.data.Username} });
                    return;
                }else{
                    cb(null, { "statusCode": util.statusCode.BAD_REQUEST, "statusMessage": "Incorrect username or password" });
                    return;
                }
            }

            let customerData = {
                "email": data.email,
                "username": data.username,
                "password": util.encryptData(data.password)
            }

            userDAO.createUser(customerData, (err, dbData) => {
                if (err) {
                    console.log(err);
                    cb(null, { "statusCode": util.statusCode.INTERNAL_SERVER_ERROR, "statusMessage": util.statusMessage.FAILED })
                    return;
                }

                cb(null, { "statusCode": util.statusCode.OK, "statusMessage": "ADDED", data: {"email": data.email, "username": data.username} });

            });
        }]
    }, (err, response) => {
        callback(response.createUserinDB);
    });
}





module.exports = {
    signup: signup
};
